import { Box, Button, Checkbox, Spacer } from "@chakra-ui/react";
import { useState } from "react";
import { TodoItem } from "src/modules/todo/types";
import { DeleteIcon } from "src/shared/design-system";

type ToDoListItemsProps = {
    item: TodoItem; 
    removeItem: (id: number) => void
}
export function ToDoListItems({ item, removeItem } : ToDoListItemsProps) {
    const [isHovered, setIsHovered] = useState(false);
    return (
        <Box 
            display='flex'
            justifyContent='space-between'
            alignItems='center'
            p={1}
            _hover={{
                backgroundColor: "#ECF1F7",
            }}
            onMouseEnter={() => setIsHovered(true)}
            onMouseLeave={() => setIsHovered(false)}
            >
            <Box padding={3} display='flex'>
                <Checkbox marginRight={3} isChecked={item.isCompleted}/>
                <p>{item.description}</p>
            </Box>
            <Spacer/>
            {isHovered && 
                <Button
                    colorScheme="red" 
                    variant="outline"
                    p={1}
                    borderRadius={5}
                    bg="red"
                    _hover={{ bg: "darkred" }}
                    onClick={() => removeItem(item.id)} 
                    >
                    <DeleteIcon color="white"/>
                </Button>
            }
        </Box>
        )
}