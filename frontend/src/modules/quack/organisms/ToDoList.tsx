import { TodoItem } from "src/modules/todo/types";
import { ToDoListItems } from "../molecules";
import { Box, Stack, Tabs, Tab, TabList } from "@chakra-ui/react";

type ToDoListProps = {
    items: TodoItem[];
    currentFilter: number;
    onFilter: (index: number) => void;
    removeItem: (id: number) => void
}

export function ToDoList({ items, currentFilter, onFilter, removeItem }: ToDoListProps  ) {
    return (
        <>
        <Tabs
        index={currentFilter}
        onChange={(index) => onFilter(index)}
        variant="soft-rounded"
        colorScheme="blue"
        my="4"
      >
        <TabList>
          <Tab>All</Tab>
          <Tab>Completed</Tab>
          <Tab>Not completed</Tab>
        </TabList>
      </Tabs>
        <Box borderWidth='1px' borderRadius='lg'>
            <Stack spacing='10px'>
                {items.map((item: any) => (
                <ToDoListItems key={item.id} item={item} removeItem={removeItem}/>
            ))}    
            </Stack>  
        </Box>
        </>
    )
}