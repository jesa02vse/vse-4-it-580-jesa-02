import { useTodoList } from 'src/modules/todo/hooks';
import { Box, Button, Heading, Input } from 'src/shared/design-system';
import { ToDoList } from 'src/modules/quack/organisms';
import { useState } from 'react';

export function Practical02Page() {
  const {addItem, removeItem, onFilter, currentFilter, filteredItems} = useTodoList();
  const [inputState, setInputState] = useState("");

  return (
    <Box>
      <Heading>Practical 02</Heading>
      <form
        onSubmit={(event) => {
          event.preventDefault();
          addItem({
            description: inputState,
            isCompleted: false
          })
          setInputState(""); 
        }}
      >
        <Input 
          type="text" 
          placeholder="What shell to be done?"
          value={inputState}
          onChange={(e) => setInputState(e.target.value)} />
        <Button type="submit">Add</Button>
      </form>
      <ToDoList 
        items={filteredItems.items}
        currentFilter={currentFilter}
        onFilter={onFilter} 
        removeItem={removeItem}/>
    </Box>
  );
}
