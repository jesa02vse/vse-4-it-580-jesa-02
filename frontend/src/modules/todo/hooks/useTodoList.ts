import { useState } from 'react';
import { type TodoItem, type TodoItemId } from '../types';

export const STATES = [
  'all',
  'completed',
  'not-completed',
] as const;

export function useTodoList() {
  const [state, setState] = useState({items: INITIAL_ITEMS, index: 4});
  const [filteredItems, setFilteredItems] = useState(state);
  const [currentFilter, setCurrentFilter] = useState(0);

  const addItem = (item: Omit<TodoItem, 'id'>) => {
    setState((prevItems) => {
      const newState = {
        items: [
          ...prevItems.items,
          {
            description: item.description,
            isCompleted: item.isCompleted,
            id: prevItems.index  
          }
        ],
        index: prevItems.index + 1
      }
      setFilteredItems(newState);
      setCurrentFilter(0);
      return newState;
    })
  };

  const setItemIsCompleted = (id: TodoItemId, isCompleted: boolean) => {
    setState((state) => ({
      ...state,
      items: state.items.map((item) => {
        if (item.id !== id) return item;

        return {
          ...item,
          isCompleted,
        };
      }),
    }));
  };

  const removeItem = (id: TodoItemId) => {
    setState((prevItems) => {
      const newItems = prevItems.items.filter(item => item.id !== id)
      const newState = {
        items: newItems,
        index: prevItems.index
      }
      setFilteredItems(newState);
      return newState
    })
  };

  const onFilter = (index: number) => {
    setCurrentFilter(index);
    setFilteredItems({
      items: state.items.filter(item => {
        switch (STATES[index]) {
          case "all":
            return true;
          case "completed":
            return item.isCompleted
          case "not-completed":
            return !item.isCompleted
        }
      }),
      index: state.index
    })
  }

  return {
    state,
    currentFilter,
    filteredItems,
    setCurrentFilter,
    addItem,
    setItemIsCompleted,
    removeItem,
    onFilter
  };
}

const INITIAL_ITEMS: Array<TodoItem> = [
  {
    id: 1,
    description: 'go grocery shopping',
    isCompleted: true,
  },
  {
    id: 2,
    description: 'wash the dishes',
    isCompleted: true,
  },
  {
    id: 3,
    description: 'write some React code',
    isCompleted: false,
  },
];

const INITIAL_STATE = {
  items: INITIAL_ITEMS,
  nextId: Math.max(...INITIAL_ITEMS.map(({ id }) => id)) + 1,
};
